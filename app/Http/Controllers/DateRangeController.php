<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class DateRangeController extends Controller
{
    public function index(Request $request)
    {
        if (request()->ajax()) {
            if (!empty($request->start_date)) {
                $data = DB::table('events')
                    ->whereBetween('start_date', array($request->start_date, $request->end_date))
                    ->get();
            } else {
                $data = DB::table('events')
                    ->get();
            }
            return datatables()->of($data)->make(true);
        }
        return view('daterange');
    }
}
