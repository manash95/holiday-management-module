<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <title>Edit Holidays</title>
</head>
<body>
    <div class="container">
        <div class="jumbotron">
            <form action="{{action('HolidayController@update', $id)}}" method="POST">

                    {{ csrf_field() }}

                <div class="jumbotron">
                    <h2><strong> Update holiday info </strong></h2>
                    <hr>

                    <input type="hidden" name="_method" value="Update">

                <div class="form-group">
                    <label> Name of the holiday</label>
                    <input type="text" class="form-control" name="title" placeholder="Enter name" value="{{$events->title}}">
                </div>
                <div class="form-group">
                    <label> Event Color</label>
                    <input type="color" class="form-control" name="color" placeholder="select color" value="{{$events->color}}">
                </div>
                <div class="form-group">
                    <label> Start date of the holiday</label>
                    <input type="date" class="form-control" name="start_date" placeholder="Enter start date" value="{{$events->start_date}}">
                </div>
                <div class="form-group">
                    <label> End date of the holiday</label>
                    <input type="date" class="form-control" name="end_date" placeholder="Enter end date" value="{{$events->end_date}}">
                </div>

                {{ method_field('PUT') }}

                <button type="submit" name="submit" class="btn btn-success">Update data</button>

                <a href="/displaydata" class="btn btn-danger">Back</a>

                </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>