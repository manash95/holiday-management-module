<html>
 <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Daterange Filter</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>  
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
 </head>
 <body>
  <div class="container"> 
    <div class="col-md-10 col-md-offset-1">   
     <br />
     <div class="page-header">
        <h4><strong>Daterange Filter</strong></h4>
     </div>
            <br />

                <div class="row input-daterange">
                    <div class="col-md-4">
                        <input type="text" name="start_date" id="start_date" class="form-control" placeholder="From Date" readonly />
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="end_date" id="end_date" class="form-control" placeholder="To Date" readonly />
                    </div>
                    <div class="col-md-4">
                        <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                        <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                    </div>
                </div>
                <br />
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="holiday_table">
                        <thead>
                            <tr>
                                <th> Id </th>
                                <th> Title </th>
                                <th> Start Date </th>
                                <th> End Date </th>       
                            </tr>
                        </thead>
                    </table>
                </div>
                <a href="/events" class="btn btn-danger">Back</a>
    </div>
  </div>
 </body>
</html>

<script>
    $(document).ready(function(){
    $('.input-daterange').datepicker({
    todayBtn:'linked',
    format:'yyyy-mm-dd',
    autoclose:true
    });

    load_data();

    function load_data(start_date = '', end_date = '')
    {
    $('#holiday_table').DataTable({
    processing: true,
    serverSide: true,
    ajax: {
        url:'{{ route("daterange.index") }}',
        data:{start_date:start_date, end_date:end_date}
    },
    columns: [
        {
        data:'id',
        name:'id'
        },
        {
        data:'title',
        name:'title'
        },
        {
        data:'start_date',
        name:'start_date'
        },
        {
        data:'end_date',
        name:'end_date'
        },
    ]
    });
    }

    $('#filter').click(function(){
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        if(start_date != '' &&  end_date != '')
        {
            $('#holiday_table').DataTable().destroy();
            load_data(start_date, end_date);
        }
        else
        {
            alert('Both Dates are required');
        }
    });

    $('#refresh').click(function(){
        $('#start_date').val('');
        $('#end_date').val('');
        $('#holiday_table').DataTable().destroy();
        load_data();
    });

    });
</script>
