<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Event</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

</head>
<body>
        <div class="container">
            <div class="jumbotron">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background: #2e6da4; color: white;">
                                Add Holiday to Calendar
                            </div>
        
                            <div class="panel-body">
                                <h2 align="center" ><strong> Insert holiday information</strong></h2>
                                <br>
                            <form method="post" action="{{action('HolidayController@store')}}" >
                            
                            {{ csrf_field() }}

                            <label for="">Enter the Name of the Holiday</label>
                            <input type="text" class="form-control" name="title" placeholder="Enter the Holiday" /><br /><br />

                            <label for="">Enter the color code</label>
                            <input type="color" class="form-control" name="color" placeholder="Enter the color" /><br /><br />

                            <label for="">Enter the start date of the holiday</label>
                            <input type="date" class="form-control" name="start_date" placeholder="Enter start date" /><br /><br />

                            <label for="">Enter the end date of the holiday</label>
                            <input type="date" class="form-control" name="end_date" placeholder="Enter end date" /><br /><br />

                            <input type="submit" name="submit" class="btn btn-primary" value="Add Event Data" />

                            <a href="/events" class="btn btn-danger">Back</a>

                            </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>
</html>