<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <title>All Events</title>

</head>
<body>

    <div class="container">
        <div class="jumbotron">

            <table class="table table-striped table-bordered table-hover">
                <thead class="thead">
                    <tr class="warning">
                        <th> Id </th>
                        <th> Title </th>
                        <th> Color </th>
                        <th> Start Date </th>
                        <th> End Date </th>
                        <th> Update </th>
                        <th> Delete </th>
                    </tr>
                </thead>
                @foreach($events as $event)
                <tbody>
                    <tr>
                        <td>{{ $event->id }}</td>
                        <td>{{ $event->title }}</td>
                        <td>{{ $event->color }}</td>
                        <td>{{ $event->start_date }}</td>
                        <td>{{ $event->end_date }}</td>

                    <th><a href="{{action('HolidayController@edit',$event['id'])}}" class="btn btn-success">
                            Edit</a>
                    </th>

                    <th>
                        <form method="POST" action="{{action('HolidayController@destroy',$event['id'])}}">
                            
                            {{ csrf_field() }}

                            <input type="hidden" name="_method" value="Delete"/>
                            <button type="submit" class="btn btn-danger"> Delete </button>

                        </form>
                    </th>


                    </tr>
                </tbody>     
                @endforeach
            </table>
            <a href="/events" class="btn btn-danger">Back</a>
        </div>
    </div>
    
</body>
</html>