<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/events', 'HolidayController');
Route::get('/addevent', 'HolidayController@display');

Route::get('/displaydata', 'HolidayController@show');

Route::get('/deleteevent', 'HolidayController@show');

Route::resource('/daterange', 'DateRangeController');
